#pragma once

#include <malloc.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t w, h;
  struct pixel* massive;
};

enum new_result{
  NEW_OK = 0,
  NEW_WRONG
};

enum new_result new_image(uint64_t w, uint64_t h, struct image* img_ptr);
void free_image(struct image* img);
void set_massive(struct image* img, struct pixel* massive);
