#pragma once
#include "image.h"

enum rotate_res{
    ROTATE_OKAY,
    ROTATE_NEW_IMAGE_ERR
};

enum rotate_res rotate(struct image const src, struct image* rotated);
