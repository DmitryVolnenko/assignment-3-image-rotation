#pragma once

#include <stdio.h>

enum show_res{
    SHOW_OK = 0,
    SHOW_WRONG
};

enum show_res show_file(const char* filetitle, const char* mode, FILE** file_ptr_ptr);

enum shut_res{
    SHUT_OK = 0,
    SHUT_WRONG
};

enum shut_res shut_file(FILE* fileptr);
