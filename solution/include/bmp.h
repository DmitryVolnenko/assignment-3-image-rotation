#pragma once
#include "image.h"
#define DEFAULT_BF_TYPE 0x4d42
#define DEFAULT_BF_TYPE2 0x4349
#define DEFAULT_BF_TYPE3 0x5450
#define DEFAULT_BF_RESERVED 0
#define DEFAULT_BI_PLANES 1
#define DEFAULT_BI_SIZE 40
#define DEFAULT_B_OFF_BITS ((DEFAULT_BI_SIZE) + 14)
#define DEFAULT_BI_COUNT 24
#define DEFAULT_BI_COMPRESSION 0
#define DEFAULT_BI_SIZE_IMAGE 0
#define DEFAULT_BI_X_PELS_PER_METER 0
#define DEFAULT_BI_Y_PELS_PER_METER 0
#define DEFAULT_BI_CLR_IMPORTANT 0
#define DEFAULT_BI_CLR_USED 0



struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};


enum i_status  {
  I_OK = 0,
  I_INVALID_TYPE,
  I_INVALID_BIT_AMOUNT,
  I_INVALID_HEADER,
  I_MALLOC_MASSIVE_ERROR,
  I_MASSIVE_ERROR,
  I_COMPRESSION_ERROR,
  I_BI_AMOUNT_ERROR,
  I_BI_PLANES_ERROR,
  I_NEW_IMAGE_ERROR
};

enum i_status from_bmp( FILE* in, struct image* img );


enum  o_status  {
  O_OK = 0,
  O_HEADER_ERROR,
  O_MASSIVE_ERROR,
  O_MALLOC_MASSIVE_ERROR
};

enum o_status to_bmp( FILE* out, struct image const* img );
