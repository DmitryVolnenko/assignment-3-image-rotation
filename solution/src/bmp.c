#include "../include/bmp.h"

enum o_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header head;
    size_t row_amount = (3*img->w + 3) & (-4);
    head.biWidth = img->w;
    head.biHeight = img->h;
    head.bfType = DEFAULT_BF_TYPE;
    head.bOffBits = DEFAULT_B_OFF_BITS;
    head.bfReserved = DEFAULT_BF_RESERVED;
    head.bfileSize = sizeof(struct bmp_header) + row_amount*img->h;
    head.biPlanes = DEFAULT_BI_PLANES;
    head.biSize = DEFAULT_BI_SIZE;
    head.biBitCount = DEFAULT_BI_COUNT;
    head.biYPelsPerMeter = DEFAULT_BI_Y_PELS_PER_METER;
    head.biClrUsed = DEFAULT_BI_CLR_USED;
    head.biXPelsPerMeter = DEFAULT_BI_X_PELS_PER_METER;
    head.biCompression = DEFAULT_BI_COMPRESSION;
    head.biSizeImage = DEFAULT_BI_SIZE_IMAGE;
    head.biClrImportant = DEFAULT_BI_CLR_IMPORTANT;
    

    size_t res = fwrite(&head, 1, sizeof(struct bmp_header), out);
    if (res != sizeof(struct bmp_header)){
        return O_HEADER_ERROR;
    }

    uint8_t* massive = malloc(row_amount*img->h);
    if (massive == NULL) return O_MALLOC_MASSIVE_ERROR;
    size_t padd = row_amount - 3*img->w;
    for (size_t i = 0; i < img->h;i++){
        for (size_t j = 0; j < img->w;j++){
            struct pixel current_pxl = img->massive[i*img->w+j];
            massive[i*row_amount + j*3] = current_pxl.b;
            massive[i*row_amount+j*3+1] = current_pxl.g;
            massive[i*row_amount+j*3+2] = current_pxl.r;
        }
        for (size_t a = 0; a < padd;a++) massive[i*row_amount + 3*img->w + a] = 0; 
                                                                                
    }
    res = fwrite(massive, 1, row_amount*img->h, out);
    free(massive);
    if (res == (row_amount*img->h)){
        return O_OK;
    }
    return O_MASSIVE_ERROR;
}


enum i_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header head;
    size_t res = fread(&head, 1, sizeof(struct bmp_header), in);
    if (res != sizeof(struct bmp_header)){
        return I_INVALID_HEADER;
    } else if (head.biCompression != DEFAULT_BI_COMPRESSION){
        return I_COMPRESSION_ERROR;
    } else if (head.biPlanes != DEFAULT_BI_PLANES){
        return I_BI_PLANES_ERROR;
    } else if (head.bfType != DEFAULT_BF_TYPE && head.bfType != DEFAULT_BF_TYPE2 && head.bfType != DEFAULT_BF_TYPE3){
        return I_INVALID_TYPE;
    } else if (head.biSize != DEFAULT_BI_SIZE && head.biSize != 108 && head.biSize != 124){
        return I_BI_AMOUNT_ERROR;
    } else if (head.biBitCount != DEFAULT_BI_COUNT){
        return I_INVALID_BIT_AMOUNT;
    }
   
    enum new_result new_res = new_image(head.biWidth, head.biHeight, img);
    if (new_res == NEW_WRONG) return I_NEW_IMAGE_ERROR;
    size_t row_amount = (head.biWidth*3 + 3) & (-4);
    uint8_t* massive = malloc(row_amount * head.biHeight);
    if (massive == NULL) return I_MALLOC_MASSIVE_ERROR;
    res = fread(massive, 1, row_amount*head.biHeight, in);
    if (res != (row_amount*head.biHeight)){
        free(massive);
        free(img);
        return I_MASSIVE_ERROR;
    }
    for (size_t i = 0;i < head.biHeight;i++){
        for (size_t j = 0; j < head.biWidth;j++){
            struct pixel current_pxl = img->massive[i*head.biWidth + j];
            current_pxl.r = massive[i*row_amount+3*j + 2];
            current_pxl.g = massive[i*row_amount+3*j + 1];
            current_pxl.b = massive[i*row_amount+3*j];
            img->massive[i*head.biWidth + j] = current_pxl;
        }
    }
    free(massive);
    return I_OK;
}

