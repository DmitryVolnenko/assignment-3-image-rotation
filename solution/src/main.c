#include "../include/utils.h"
#include "../include/bmp.h"
#include "../include/transform.h"


int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Valid amount of args is 3.\n");
        return 0;
    }
    FILE* file_in_ptr;
    enum show_res show_res_in = show_file(argv[1], "r", &file_in_ptr);
    if (show_res_in == SHOW_WRONG){
        printf("Can't open %s.\n", argv[1]);
        return 0;
    }
    printf("File %s opened!\n", argv[1]);

    struct image img;
    enum i_status i_res = from_bmp(file_in_ptr, &img);

    if (i_res == I_INVALID_HEADER){
        shut_file(file_in_ptr);
        printf("Cant read header.\n");
        return 0;
    } else if (i_res == I_INVALID_TYPE){
        shut_file(file_in_ptr);
        printf("Invalid type of bmp file.\n");
        return 0;
    }  else if (i_res == I_INVALID_BIT_AMOUNT){
        shut_file(file_in_ptr);
        printf("Wrong bmp file bit amount.\n");
        return 0;
    } else if (i_res == I_MASSIVE_ERROR){
        shut_file(file_in_ptr);
        printf("Cant read data from bmp file.\n");
        return 0;
    } else if (i_res == I_MALLOC_MASSIVE_ERROR){
        shut_file(file_in_ptr);
        printf("Cant malloc enough memory for massive.\n");
        return 0;
    } else if (i_res == I_BI_PLANES_ERROR){
        shut_file(file_in_ptr);
        printf("Invalid biPlanes config.\n");
        return 0;
    } else if (i_res == I_BI_AMOUNT_ERROR){
        shut_file(file_in_ptr);
        printf("Invalid biSize parameter.\n");
        return 0;
    } else if (i_res == I_COMPRESSION_ERROR){
        shut_file(file_in_ptr);
        printf("File is compressed.\n");
        return 0;

    } else if (i_res == I_NEW_IMAGE_ERROR){
        shut_file(file_in_ptr);
        printf("Error occured while creating image.\n");
        return 0;
    }
    printf("Starting to transform!\n");
    enum shut_res shut_res_in = shut_file(file_in_ptr);
    if (shut_res_in == SHUT_WRONG){
        printf("Can't close %s.\n", argv[1]);
        free_image(&img);
        return 0;
    }
    printf("File %s closed!\n", argv[1]);

    struct image rotated_img;
    enum rotate_res rotate_result = rotate(img, &rotated_img);
    if (rotate_result == ROTATE_NEW_IMAGE_ERR){
        free_image(&img);
        printf("Can not create image during rotate.\n");
        return 0;
    }
    printf("Successfully transformed!\n");
    free_image(&img);

    FILE* file_out_ptr;
    enum show_res show_res_out = show_file(argv[2], "wb", &file_out_ptr);
    if (show_res_out == SHOW_WRONG){
        printf("Can't open %s. \n", argv[2]);
        return 0;
    }
    printf("File %s opened!\n", argv[2]);

    enum o_status o_result = to_bmp(file_out_ptr, &rotated_img);
    free_image(&rotated_img);
    if (o_result == O_HEADER_ERROR){
        printf("Cant write header.\n");
        shut_file(file_out_ptr);
        return 0;
    } else if (o_result == O_MASSIVE_ERROR){
        printf("Cant write massive.\n");
        shut_file(file_out_ptr);
        return  0;
    } else if (o_result == O_MALLOC_MASSIVE_ERROR){
        printf("Cant malloc enough memory for massive.\n");
        shut_file(file_out_ptr);
        return 0;
    }
    printf("Writed successfully!\n");
    enum shut_res shut_res_out = shut_file(file_out_ptr);
    if (shut_res_out == SHUT_WRONG){
        printf("Can't close %s. \n", argv[2]);
        return 0;
    }
    printf("File %s closed!\n", argv[2]);


    return 0;
}
