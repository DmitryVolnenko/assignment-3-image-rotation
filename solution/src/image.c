#include "../include/image.h"


void free_image(struct image* img){
    free(img->massive);
}

enum new_result new_image(uint64_t w, uint64_t h, struct image* img_ptr){
    img_ptr->w = w;
    img_ptr->h = h;
    img_ptr->massive = malloc(sizeof(struct pixel) * h * w);
    if (img_ptr->massive != NULL) return NEW_OK;
    return NEW_WRONG;
}


