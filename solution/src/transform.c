#include "../include/transform.h"

enum rotate_res rotate(struct image const src, struct image* rotated){
    enum new_result new_res = new_image(src.h, src.w, rotated);
    if (new_res == NEW_WRONG) return ROTATE_NEW_IMAGE_ERR;
    for (size_t i = 0; i < src.w;i++){
        for (size_t j = 0; j < src.h; j++){
            rotated->massive[i*rotated->w+rotated->w - j - 1] = src.massive[j*src.w+i];
        }
    }
    return ROTATE_OKAY;
}

