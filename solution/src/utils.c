#include "../include/utils.h"

enum show_res show_file(const char* filetitle, const char* mode, FILE** file_ptr_ptr){
    *file_ptr_ptr = fopen(filetitle, mode);
    if (!*file_ptr_ptr) return SHOW_WRONG;
    return SHOW_OK;
}


enum shut_res shut_file(FILE* fileptr){
    int res = fclose(fileptr);
    if (res == EOF) return SHUT_WRONG;
    return SHUT_OK;
}
